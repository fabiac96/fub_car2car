Inhaltsverzeichnis:

- Einleitung
    - Automini 
    - Motivation
    - ...
- Theoretische Grundlagen 
    - Car2Car 
    - Ros
    - ...
- Platooning 
- Safety Driving 
- Fazit und Ausblick